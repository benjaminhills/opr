# Open Polar Radar (OPR) Toolbox
OPR Toolbox: Polar radar software toolbox (currently mostly Matlab based)

Contains code for calibrating, processing, analyzing, and viewing radar data. The tools are focused on radar sounders.

Main guide for setting up and using the cresis toolbox:
https://gitlab.com/openpolarradar/opr/-/wikis/home

matlab: Matlab code. This is the main directory with most of the source code.

idl: IDL code. Only includes support for reading data product files

python: Python code. Includes support for reading ECMWF weather model files, tape library raw data retrieval.

vim: Vim code. Only includes setup files for using vimdiff.

## To cite usage of the OPR Toolbox please use the following:

Open Polar Radar. (2023). opr (Version 3.0.1) [Computer software]. https://doi.org/10.5281/zenodo.5683959

## To acknowledge the use of the OPR Toolbox. For example:

We acknowledge the use of software from Open Polar Radar generated with support from NSF EarthCube grants 2126503, 2127606, and 2126468.
